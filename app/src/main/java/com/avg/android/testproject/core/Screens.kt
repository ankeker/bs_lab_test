package com.avg.android.testproject.core

import com.avg.android.testproject.presentation.model.GiftCard
import com.avg.android.testproject.ui.MainFlowFragment
import com.avg.android.testproject.ui.giftcard.GiftCardFragment
import com.avg.android.testproject.ui.provider.ProviderFragment
import ru.terrakok.cicerone.android.support.SupportAppScreen

object Screens {

    object MainFlow : SupportAppScreen() {
        override fun getFragment() = MainFlowFragment()
    }

    object ProviderScreen : SupportAppScreen() {
        override fun getFragment() = ProviderFragment()
    }

    data class GiftCardScreen(
        private val giftCard: GiftCard
    ) : SupportAppScreen() {
        override fun getFragment() = GiftCardFragment.newInstance(giftCard)
    }
}