package com.avg.android.testproject.ui.provider.viewholders

import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.avg.android.testproject.R
import com.avg.android.testproject.glide.GlideApp
import com.avg.android.testproject.presentation.model.GiftCard

class GiftCardViewHolder constructor(
    itemView: View
) : RecyclerView.ViewHolder(itemView) {

    private val imageView = itemView.findViewById<ImageView>(R.id.gift_card_image_item_view)
    private val priceView = itemView.findViewById<TextView>(R.id.gift_card_price_item_view)
    private val countCoinsView = itemView.findViewById<TextView>(R.id.gift_card_count_item_view)
    private val coinsContainer =
        itemView.findViewById<LinearLayout>(R.id.gift_card_count_item_container)

    fun bindView(item: GiftCard) {
        with(item) {
            GlideApp.with(itemView)
                .load(imageUrl)
                .into(imageView)

            priceView.text = title.substring(0, title.indexOf(" "))
            countCoinsView.text = codesCount.toString()

            coinsContainer.setBackgroundResource(
                if (isEvenProvider) {
                    R.color.outer_space
                } else {
                    R.color.orange_peel
                }
            )
        }
    }
}