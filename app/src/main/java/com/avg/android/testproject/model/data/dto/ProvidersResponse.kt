package com.avg.android.testproject.model.data.dto

import com.google.gson.annotations.SerializedName

data class ProviderResponse(
    @SerializedName("providers")
    val providers: List<Provider>
)