package com.avg.android.testproject.model.repository

import com.avg.android.testproject.model.data.dto.ProviderResponse
import com.avg.android.testproject.model.data.server.ProviderService
import com.avg.android.testproject.model.system.scheduler.SchedulersProvider
import io.reactivex.Single
import javax.inject.Inject

class ProviderRepository @Inject constructor(
    private val service: ProviderService,
    private val schedulers: SchedulersProvider
) {

    fun getProviders(): Single<ProviderResponse> = service.getProviders()
        .subscribeOn(schedulers.io())
        .observeOn(schedulers.ui())
}