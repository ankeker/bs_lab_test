package com.avg.android.testproject.presentation.provider

import android.content.Context
import android.widget.Toast
import com.avg.android.testproject.core.BasePresenter
import com.avg.android.testproject.core.ErrorHandler
import com.avg.android.testproject.core.FlowRouter
import com.avg.android.testproject.core.Screens
import com.avg.android.testproject.model.interactor.main.ProviderInteractor
import com.avg.android.testproject.presentation.model.GiftCard
import moxy.InjectViewState
import javax.inject.Inject

@InjectViewState
class ProviderPresenter @Inject constructor(
    private val flowRouter: FlowRouter,
    private val errorHandler: ErrorHandler,
    private val interactor: ProviderInteractor,
    private val context: Context
) : BasePresenter<ProviderView>() {

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()

        interactor.getProviders()
            .doOnSubscribe { viewState.showProgress() }
            .doAfterTerminate { viewState.hideProgress() }
            .subscribe(
                { viewState.populateView(it) },
                {
                    errorHandler.handleError(it) { message ->
                        Toast.makeText(context, message, Toast.LENGTH_LONG).show()
                    }
                }
            )
            .connect()
    }

    fun onItemClicked(giftCard: GiftCard) {
        flowRouter.navigateTo(Screens.GiftCardScreen(giftCard))
    }

    fun onBackPressed() {
        flowRouter.finishFlow()
    }
}