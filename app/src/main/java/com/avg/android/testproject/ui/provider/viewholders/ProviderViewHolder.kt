package com.avg.android.testproject.ui.provider.viewholders

import android.view.View
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.avg.android.testproject.R
import com.avg.android.testproject.model.data.dto.Provider
import com.avg.android.testproject.presentation.model.GiftCard
import com.avg.android.testproject.ui.provider.adapters.GiftCardAdapter

class ProviderViewHolder(
    itemView: View,
    clickListener: (item: GiftCard) -> Unit
) : RecyclerView.ViewHolder(itemView) {

    private val adapter = GiftCardAdapter { clickListener(it) }
    private val headerView = itemView.findViewById<TextView>(R.id.provider_header_view)

    init {
        itemView.findViewById<RecyclerView>(R.id.provider_recycler_view).adapter = adapter
    }

    fun bindView(item: Provider, isEvenProvider: Boolean) {
        headerView.text = item.title
        adapter.setItems(item.giftCardResponses, isEvenProvider)
    }
}