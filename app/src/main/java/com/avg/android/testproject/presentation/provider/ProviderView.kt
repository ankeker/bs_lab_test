package com.avg.android.testproject.presentation.provider

import com.avg.android.testproject.model.data.dto.Provider
import moxy.MvpView
import moxy.viewstate.strategy.AddToEndSingleStrategy
import moxy.viewstate.strategy.StateStrategyType

@StateStrategyType(AddToEndSingleStrategy::class)
interface ProviderView : MvpView {

    fun populateView(providers: List<Provider>)

    fun showProgress()

    fun hideProgress()
}