package com.avg.android.testproject.ui.giftcard

import android.os.Bundle
import android.view.View
import com.avg.android.testproject.R
import com.avg.android.testproject.core.BaseFragment
import com.avg.android.testproject.glide.GlideApp
import com.avg.android.testproject.presentation.giftcard.GiftCardPresenter
import com.avg.android.testproject.presentation.giftcard.GiftCardView
import com.avg.android.testproject.presentation.model.GiftCard
import kotlinx.android.synthetic.main.fragment_gift_card.*
import moxy.presenter.InjectPresenter
import moxy.presenter.ProvidePresenter
import toothpick.Scope
import toothpick.config.Module

class GiftCardFragment : BaseFragment(), GiftCardView {

    companion object {
        private const val GIFT_CARD_EXTRAS = "GIFT_CARD_EXTRAS"

        fun newInstance(giftCard: GiftCard) = GiftCardFragment().apply {
            arguments = Bundle().apply {
                putParcelable(GIFT_CARD_EXTRAS, giftCard)
            }
        }
    }

    override val layoutRes = R.layout.fragment_gift_card

    override fun installScopeModules(scope: Scope) {
        scope.installModules(object : Module() {
            init {
                bind(GiftCard::class.java).toInstance(
                    arguments?.getParcelable(GIFT_CARD_EXTRAS)
                        ?: throw IllegalArgumentException("${javaClass.simpleName} need GiftCard as argument")
                )
            }
        })
    }

    @InjectPresenter
    lateinit var presenter: GiftCardPresenter

    @ProvidePresenter
    fun providePresenter(): GiftCardPresenter =
        scope.getInstance(GiftCardPresenter::class.java)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        gift_card_toolbar.setNavigationOnClickListener { presenter.onBackPressed() }
    }

    override fun populateView(giftCard: GiftCard) {
        with(giftCard) {
            GlideApp.with(requireContext())
                .load(imageUrl)
                .into(gift_card_image_view)

            gift_card_price_view.text = title.substring(0, title.indexOf(" "))
            gift_card_count_view.text = codesCount.toString()
            gift_card_description_view.text = description

            gift_card_count_container.setBackgroundResource(
                if (isEvenProvider) {
                    R.color.outer_space
                } else {
                    R.color.orange_peel
                }
            )
        }
    }

    override fun onBackPressed() {
        presenter.onBackPressed()
    }
}