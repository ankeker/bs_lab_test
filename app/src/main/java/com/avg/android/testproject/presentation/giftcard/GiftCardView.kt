package com.avg.android.testproject.presentation.giftcard

import com.avg.android.testproject.presentation.model.GiftCard
import moxy.MvpView
import moxy.viewstate.strategy.AddToEndSingleStrategy
import moxy.viewstate.strategy.StateStrategyType

@StateStrategyType(AddToEndSingleStrategy::class)
interface GiftCardView : MvpView {

    fun populateView(giftCard: GiftCard)
}