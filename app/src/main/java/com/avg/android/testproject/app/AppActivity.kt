package com.avg.android.testproject.app

import android.os.Bundle
import androidx.core.view.isGone
import com.avg.android.testproject.BuildConfig
import com.avg.android.testproject.R
import com.avg.android.testproject.app.presentation.AppPresenter
import com.avg.android.testproject.app.presentation.AppView
import com.avg.android.testproject.core.FlowFragment
import com.avg.android.testproject.di.DI
import kotlinx.android.synthetic.main.activity_app.*
import moxy.MvpAppCompatActivity
import moxy.presenter.InjectPresenter
import moxy.presenter.ProvidePresenter
import ru.terrakok.cicerone.Navigator
import ru.terrakok.cicerone.NavigatorHolder
import ru.terrakok.cicerone.android.support.SupportAppNavigator
import toothpick.Toothpick
import javax.inject.Inject

class AppActivity : MvpAppCompatActivity(), AppView {

    @InjectPresenter
    lateinit var presenter: AppPresenter

    @Inject
    lateinit var navigatorHolder: NavigatorHolder

    private val navigator: Navigator =
        SupportAppNavigator(this, supportFragmentManager, R.id.appContainer_fl)

    private val currentFlowFragment: FlowFragment?
        get() = supportFragmentManager.findFragmentById(R.id.appContainer_fl) as? FlowFragment

    @ProvidePresenter
    fun providePresenter(): AppPresenter =
        Toothpick.openScope(DI.APP_SCOPE).getInstance(AppPresenter::class.java)

    override fun onCreate(savedInstanceState: Bundle?) {
        Toothpick.inject(this, Toothpick.openScope(DI.SERVER_SCOPE))

        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_app)

        if (BuildConfig.DEBUG && savedInstanceState == null) {
            versionView.postDelayed(
                {
                    versionView.isGone = true
                    presenter.onAppStarted()
                },
                DELAY_VERSION_VIEW
            )
        } else {
            versionView.isGone = true
        }
    }

    override fun onResumeFragments() {
        super.onResumeFragments()

        navigatorHolder.setNavigator(navigator)
    }

    override fun onPause() {
        navigatorHolder.removeNavigator()

        super.onPause()
    }

    override fun onDestroy() {
        super.onDestroy()

        versionView.handler.removeCallbacksAndMessages(null)
    }

    override fun onBackPressed() {
        currentFlowFragment?.onBackPressed() ?: presenter.onBackPressed()
    }

    companion object {
        private const val DELAY_VERSION_VIEW = 1000L
    }
}