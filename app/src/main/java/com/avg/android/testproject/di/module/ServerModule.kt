package com.avg.android.testproject.di.module

import com.avg.android.testproject.di.provider.GsonProvider
import com.avg.android.testproject.di.provider.OkHttpClientProvider
import com.avg.android.testproject.di.provider.ServerApiProvider
import com.avg.android.testproject.model.data.server.ProviderService
import com.google.gson.Gson
import okhttp3.OkHttpClient
import toothpick.config.Module

class ServerModule : Module() {

    init {
        // Network
        bind(OkHttpClient::class.java).toProvider(OkHttpClientProvider::class.java)
            .providesSingletonInScope()
        bind(Gson::class.java).toProvider(GsonProvider::class.java).providesSingletonInScope()
        bind(ProviderService::class.java).toProvider(ServerApiProvider::class.java)
            .providesSingletonInScope()
    }
}