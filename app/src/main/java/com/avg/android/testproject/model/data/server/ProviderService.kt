package com.avg.android.testproject.model.data.server

import com.avg.android.testproject.model.data.dto.ProviderResponse
import io.reactivex.Single
import retrofit2.http.GET

interface ProviderService {

    // TODO: Haven't done for a long time use get request without parameters. It's from google.
    @GET(".")
    fun getProviders(): Single<ProviderResponse>
}