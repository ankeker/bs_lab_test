package com.avg.android.testproject.model.interactor.main

import com.avg.android.testproject.model.repository.ProviderRepository
import javax.inject.Inject

class ProviderInteractor @Inject constructor(
    private val providerRepository: ProviderRepository
) {

    fun getProviders() = providerRepository.getProviders()
        .map { it.providers }
}