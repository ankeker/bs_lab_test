package com.avg.android.testproject.core

import ru.terrakok.cicerone.Router

class FlowRouter(
    private val appRouter: Router
) : Router() {

    fun finishFlow() {
        appRouter.exit()
    }
}