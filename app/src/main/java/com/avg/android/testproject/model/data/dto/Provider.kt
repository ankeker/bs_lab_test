package com.avg.android.testproject.model.data.dto

import com.google.gson.annotations.SerializedName

data class Provider(
    @SerializedName("id")
    val id: Int,
    @SerializedName("title")
    val title: String,
    @SerializedName("image_url")
    val imageUrl: String,
    @SerializedName("gift_cards")
    val giftCardResponses: List<GiftCardResponse>
)