package com.avg.android.testproject.ui.provider.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.avg.android.testproject.R
import com.avg.android.testproject.model.data.dto.GiftCardResponse
import com.avg.android.testproject.ui.provider.viewholders.GiftCardViewHolder
import com.avg.android.testproject.presentation.model.GiftCard

class GiftCardAdapter constructor(
    private val clickListener: (item: GiftCard) -> Unit
) : RecyclerView.Adapter<GiftCardViewHolder>() {

    private val items = mutableListOf<GiftCard>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): GiftCardViewHolder {
        val itemView = LayoutInflater.from(parent.context)
            .inflate(R.layout.gift_card_view, parent, false)
        return GiftCardViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: GiftCardViewHolder, position: Int) {
        val item = items[position]
        holder.bindView(item)
        holder.itemView.setOnClickListener { clickListener(item) }
    }

    override fun getItemCount() = items.size

    fun setItems(giftCardResponses: List<GiftCardResponse>, isEvenProvider: Boolean) {
        items.clear()
        items.addAll(
            giftCardResponses.map {
                with(it) {
                    GiftCard(
                        id,
                        featured,
                        title,
                        credits,
                        imageUrl,
                        codesCount,
                        currency,
                        description,
                        redeemUrl,
                        isEvenProvider
                    )
                }
            }
        )
        notifyDataSetChanged()
    }
}