package com.avg.android.testproject.di.module

import android.content.Context
import com.avg.android.testproject.app.App
import com.avg.android.testproject.core.ErrorHandler
import com.avg.android.testproject.model.system.resource.ResourceManager
import com.avg.android.testproject.model.system.scheduler.AppSchedulers
import com.avg.android.testproject.model.system.scheduler.SchedulersProvider
import ru.terrakok.cicerone.Cicerone
import ru.terrakok.cicerone.NavigatorHolder
import ru.terrakok.cicerone.Router
import toothpick.config.Module

class AppModule(
    app: App
) : Module() {

    init {
        bind(Context::class.java).toInstance(app)
        bind(SchedulersProvider::class.java).toInstance(AppSchedulers())
        bind(ResourceManager::class.java).singletonInScope()
        bind(ErrorHandler::class.java).singletonInScope()

        // Navigation
        val cicerone = Cicerone.create()
        bind(Router::class.java).toInstance(cicerone.router)
        bind(NavigatorHolder::class.java).toInstance(cicerone.navigatorHolder)
    }
}