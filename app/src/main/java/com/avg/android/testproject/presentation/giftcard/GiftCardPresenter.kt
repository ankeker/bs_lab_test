package com.avg.android.testproject.presentation.giftcard

import com.avg.android.testproject.core.BasePresenter
import com.avg.android.testproject.core.FlowRouter
import com.avg.android.testproject.core.Screens
import com.avg.android.testproject.presentation.model.GiftCard
import moxy.InjectViewState
import javax.inject.Inject

@InjectViewState
class GiftCardPresenter @Inject constructor(
    private val flowRouter: FlowRouter,
    private val giftCard: GiftCard
) : BasePresenter<GiftCardView>() {

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()

        viewState.populateView(giftCard)
    }

    fun onBackPressed() {
        flowRouter.backTo(Screens.ProviderScreen)
    }
}