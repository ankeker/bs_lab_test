package com.avg.android.testproject.ui.provider.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.avg.android.testproject.R
import com.avg.android.testproject.model.data.dto.Provider
import com.avg.android.testproject.presentation.model.GiftCard
import com.avg.android.testproject.ui.provider.viewholders.ProviderViewHolder

class ProviderAdapter constructor(
    private val clickListener: (item: GiftCard) -> Unit
) : RecyclerView.Adapter<ProviderViewHolder>() {

    private val items = mutableListOf<Provider>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProviderViewHolder {
        val itemView = LayoutInflater.from(parent.context)
            .inflate(R.layout.provider_view, parent, false)
        return ProviderViewHolder(itemView) { clickListener(it) }
    }

    override fun getItemCount() = items.size

    override fun onBindViewHolder(holder: ProviderViewHolder, position: Int) {
        holder.bindView(items[position], (position + 1) % 2 == 0)
    }

    fun setItems(providers: List<Provider>) {
        items.clear()
        items.addAll(providers)
        notifyDataSetChanged()
    }
}