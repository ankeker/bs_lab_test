package com.avg.android.testproject.presentation.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class GiftCard(
    val id: Int,
    val featured: Boolean,
    val title: String,
    val credits: Int,
    val imageUrl: String,
    val codesCount: Int,
    val currency: String,
    val description: String,
    val redeemUrl: String,
    val isEvenProvider: Boolean
) : Parcelable