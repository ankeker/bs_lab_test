package com.avg.android.testproject.ui.provider

import android.os.Bundle
import android.view.View
import com.avg.android.testproject.R
import com.avg.android.testproject.core.BaseFragment
import com.avg.android.testproject.extension.makeGone
import com.avg.android.testproject.extension.makeVisible
import com.avg.android.testproject.model.data.dto.Provider
import com.avg.android.testproject.presentation.provider.ProviderPresenter
import com.avg.android.testproject.presentation.provider.ProviderView
import com.avg.android.testproject.ui.provider.adapters.ProviderAdapter
import kotlinx.android.synthetic.main.fragment_provider.*
import moxy.presenter.InjectPresenter
import moxy.presenter.ProvidePresenter
import toothpick.Scope

class ProviderFragment : BaseFragment(), ProviderView {

    override val layoutRes = R.layout.fragment_provider

    override fun installScopeModules(scope: Scope) {
    }

    @InjectPresenter
    lateinit var presenter: ProviderPresenter

    @ProvidePresenter
    fun providePresenter(): ProviderPresenter =
        scope.getInstance(ProviderPresenter::class.java)

    private lateinit var adapter: ProviderAdapter

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        adapter = ProviderAdapter { presenter.onItemClicked(it) }
        main_recycler_view.adapter = adapter
    }

    override fun populateView(providers: List<Provider>) {
        adapter.setItems(providers)
    }

    override fun showProgress() {
        provider_container_view.makeGone()
        main_progress_view.makeVisible()
    }

    override fun hideProgress() {
        provider_container_view.makeVisible()
        main_progress_view.makeGone()
    }

    override fun onBackPressed() {
        presenter.onBackPressed()
    }
}