package com.avg.android.testproject.app.presentation

import com.avg.android.testproject.core.BasePresenter
import com.avg.android.testproject.core.ErrorHandler
import com.avg.android.testproject.core.Screens
import ru.terrakok.cicerone.Router
import javax.inject.Inject

class AppPresenter @Inject constructor(
    private val router: Router,
    private val errorHandler: ErrorHandler
) : BasePresenter<AppView>() {

    override fun onDestroy() {
        super.onDestroy()

        errorHandler.onDestroy()
    }

    fun onAppStarted() {
        router.newRootScreen(Screens.MainFlow)
    }

    fun onBackPressed() {
        router.exit()
    }
}