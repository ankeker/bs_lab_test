package com.avg.android.testproject.ui

import android.os.Bundle
import com.avg.android.testproject.core.FlowFragment
import com.avg.android.testproject.core.Screens
import com.avg.android.testproject.extension.setLaunchScreen

class MainFlowFragment : FlowFragment() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        if (childFragmentManager.fragments.isEmpty()) {
            navigator.setLaunchScreen(Screens.ProviderScreen)
        }
    }
}