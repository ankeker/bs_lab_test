**Ответы на вопросы**

1. Как организовать возможность смены стиля отображения карт?

При помощи создания selector-а в файле drawable и атрибутов: `state_selected,
    state_checked, state_enabled` и т.д. __Пример:__
    
```
<selector xmlns:android="http://schemas.android.com/apk/res/android">
    <item android:drawable="@android:color/transparent" android:state_checked="false" />
    <item android:drawable="@color/bright_blue" android:state_checked="true" />
</selector>
```

2. Как открывать детализацию определенной карты по получению PUSH-уведомления?

При получении пуша просыпается сервис, который зарегистрирован в манифесте приложения. В сервисе 
вызывается метод onMessageReceived. В Этом методе реализоваывается открытие детализации карты.

3. Как минимальными изменениями в коде загружать данные с другого сервера (формат тот же)?

* В градле прописать типы билдов и каждому прописать свой урл.
* В градле прописать несколько строк. __Пример:__.
```
    productFlavors {

        apiTest {
            dimension "url"
            buildConfigField "String", "AUTH_SERVER", '"https://url/"'
            buildConfigField "String", "SERVICE_SERVER", '"http://url2:3000/"'
        }
    }
```

* Создать `константы` в приложении и, если использовать ретрофит, подставлять в метод baseUrl().
* Через `@Qualifier`.

4. Как при этом организовать кеширование для загружаемых данных?

* `БД`
* `SharedPreference`
* `File`